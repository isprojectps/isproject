-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `costs`;
CREATE TABLE `costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL DEFAULT '1',
  `invoice_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `hour` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `invoice_id` (`invoice_id`),
  CONSTRAINT `costs_ibfk_3` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `costs` (`id`, `project_id`, `user_id`, `invoice_id`, `name`, `price`, `hour`, `date`) VALUES
(1, 1,  1,  '1',  'Cost One', 10, 4500, '2017-02-11 13:28:27'),
(11,  2,  2,  '1',  'Lorem',  600,  5,  '2017-02-11 13:30:30'),
(12,  1,  5,  '1',  'LLLL', 450,  123,  '2017-02-13 09:10:58'),
(13,  2,  2,  NULL, 'Lukas',  2500, 6,  '2017-02-13 09:25:54'),
(14,  2,  1,  '1',  'Test', 6000, 2,  '2017-02-14 21:03:37'),
(15,  1,  1,  '1',  'Nový nazev', 12345,  256,  '2017-02-16 19:13:12'),
(16,  1,  4,  '1',  'První náklad', 5000, 20, '2017-02-16 19:46:43'),
(17,  3,  4,  '1',  'Test Lukas', 1235, 5,  '2017-02-20 13:48:39'),
(19,  1,  4,  '0',  'Test Lukas', 231321, 55, '2017-02-20 13:51:24'),
(20,  1,  4,  '', 'test', 159,  1,  '2017-02-20 13:51:41'),
(21,  1,  4,  '9',  '1',  2,  3,  '2017-02-20 13:54:34'),
(22,  2,  1,  '7',  'test', 15, 1,  '2017-02-20 20:34:01'),
(23,  3,  1,  '8',  'Lorem',  256,  1,  '2017-02-21 13:38:54'),
(24,  1,  4,  '9',  'Testovací název',  1200, 5,  '2017-02-21 19:20:40'),
(25,  1,  4,  '10', 'Lorem',  1,  1,  '2017-02-22 19:18:19'),
(26,  1,  4,  NULL, 'lorem',  152,  2,  '2017-02-22 20:09:08'),
(27,  3,  4,  NULL, 'Test', 150,  5,  '2017-02-22 20:11:30'),
(28,  3,  1,  '5',  'Lorem',  152,  2,  '2017-02-22 20:40:03'),
(29,  3,  1,  '5',  'úprava menu',  500,  1,  '2017-02-22 20:43:59'),
(30,  2,  1,  NULL, 'Lorem',  1,  2,  '2017-02-25 17:02:38');

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `sup_email` varchar(60) DEFAULT NULL,
  `sup_city` varchar(60) DEFAULT NULL,
  `sup_street` varchar(60) DEFAULT NULL,
  `sup_zip` varchar(60) DEFAULT NULL,
  `sup_ic` varchar(60) DEFAULT NULL,
  `sup_dic` varchar(60) DEFAULT NULL,
  `sup_an` varchar(60) DEFAULT NULL,
  `sup_tradeoffice` varchar(60) DEFAULT NULL,
  `sup_name` varchar(60) DEFAULT NULL,
  `sub_name` varchar(60) DEFAULT NULL,
  `sub_city` varchar(60) DEFAULT NULL,
  `sub_street` varchar(60) DEFAULT NULL,
  `sub_zip` varchar(60) DEFAULT NULL,
  `sub_ic` varchar(60) DEFAULT NULL,
  `sub_dic` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `invoices` (`id`, `number`, `date`, `user_id`, `sup_email`, `sup_city`, `sup_street`, `sup_zip`, `sup_ic`, `sup_dic`, `sup_an`, `sup_tradeoffice`, `sup_name`, `sub_name`, `sub_city`, `sub_street`, `sub_zip`, `sub_ic`, `sub_dic`) VALUES
(1, '20170001', '2017-02-18 20:57:41',  1,  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2017003',  '2017-02-20 12:37:19',  1,  'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz'),
(3, '123',  '2017-02-20 12:52:45',  1,  'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '123456789',  '-',  '12345/2010', 'České Budějovice', 'Admin',  'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz', 'me@lukaspavel.cz'),
(4, '1200000',  '2017-02-20 13:08:03',  1,  'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '123456789',  '-',  '12345/2010', 'České Budějovice', 'Admin',  'Fresh Services, s.r.o.', 'Praha 4',  'Nad Ostrovem 791/8', '147 00', '28180208', 'CZ28180208'),
(5, '555',  '2017-02-20 13:27:46',  1,  'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '123456789',  '-',  '12345/2010', 'České Budějovice', 'Lukáš Pavel',  'Fresh Services, s.r.o.', 'Praha 4',  'Nad Ostrovem 791/8', '147 00', '28180208', 'CZ28180208'),
(7, '256',  '2017-02-20 20:34:14',  1,  'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '123456789',  '-',  '12345/2010', 'České Budějovice', 'Lukáš Pavel',  'Fresh Services, s.r.o.', 'Praha 4',  'Nad Ostrovem 791/8', '147 00', '28180208', 'CZ28180208'),
(8, '6565', '2017-02-21 13:39:03',  1,  'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '123456789',  '-',  '12345/2010', 'České Budějovice', 'Lukáš Pavel',  'Fresh Services, s.r.o.', 'Praha 4',  'Nad Ostrovem 791/8', '147 00', '28180208', 'CZ28180208'),
(10,  '2017001',  '2017-02-22 19:05:49',  4,  'knotdesign@seznam.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '88162346', '-',  '125',  'Lorem',  'lukas',  'Fresh Services, s.r.o.', 'Praha 4',  'Nad Ostrovem 791/8', '147 00', '28180208', 'CZ28180208');

DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `income` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `projects` (`id`, `name`, `income`) VALUES
(1, 'Projekt One',  NULL),
(2, 'Projekt Two',  NULL),
(3, 'Testovací projekt',  NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `ic` varchar(255) DEFAULT NULL,
  `dic` varchar(255) DEFAULT NULL,
  `an` varchar(255) DEFAULT NULL,
  `tradeoffice` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `role`, `email`, `city`, `street`, `zip`, `ic`, `dic`, `an`, `tradeoffice`) VALUES
(1, 'Lukáš Pavel',  '$2y$10$/1T8TQ936o/qBQS9A./Jg.PguLRPjZmOj8UO22rc2cEhqlfA2OHmi', 'admin',  'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '37311',  '12345678', '-',  '12345/2010', 'České Budějovice'),
(4, 'lukas',  '$2y$10$EBmEk/dsKB4V9FTEMeGcaOyYdpz5W1enoGy3NHllfZi.k5gt3PEOG', 'user', 'knotdesign@seznam.cz', 'Ledenice', 'Dr.Stejskala 404', '37311',  '88162346', '', '125/125',  'Lorem'),
(5, 'Lukáš',  '123',  'user', 'me@lukaspavel.cz', 'Ledenice', 'Dr.Stejskala 404', '373 11', '88162346', '', '123456789',  'Les'),
(6, 'Lukáš',  '$2y$10$UOUFIev0My44sFaeN.BoIu2slqlJMORs1qjzpIL3uABKwfjschuBO', 'user', 'knotxxx@seznam.cz',  'Ledenice', 'Dr.Stejskala 404', '373 11', '88162346', '-',  '125',  '4B');

-- 2017-02-26 10:28:21
