<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class InvoicePresenter extends BasePresenter
{

    private $invoice;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    /** 
    * render table of invoices
    */
    public function renderDefault()
	{
        $user = $this->getUser();

        $invoices = $this->database->table('invoices')
            ->order('date DESC'); 

        if (!$user->isInRole('admin')) { // pokud není uživatel v roli admina zobraz jen jeho faktury
            $id = $user->getIdentity()->getId();
            $invoices->where('user_id', $id); 
        }
        
        $this->template->invoices = $invoices; 
        
	}
    /** 
    * render detail of invoice 
    * @param $id - id of invoice
    */
    public function renderDetail($id)
    {
        $invoice = $this->database->table('invoices')->get($id);
        $this->template->invoice = $invoice;

        $this->template->invoiceItems = $this->database->table('costs')->where('invoice_id', $id);
        $this->template->totalPrice = $this->database->table('costs')->where('invoice_id', $id)->sum('price');

        $date = clone $invoice->date;
        $date->modify('+14 days');
        $this->template->expirationdate = $date;
    }
    /** 
    * Form create/edit invoice
    */
    protected function createComponentInvoiceForm()
    {
        $form = new Form; // means Nette\Application\UI\Form

        $user = $this->getUser();
        $userIdentity = $this->database->table('users')->get($this->user->id);

        $form->addHidden('user_id', $user->getIdentity()->getId() );

        $form->addText('number', 'Číslo faktury - např: 20170001')
            ->addRule(Form::INTEGER, 'Číslo faktury musí být číslo')
            ->addRule(Form::MIN, 'Číslo faktury musí být větší než 0', 0)
            ->setAttribute('class', 'form-control')
            ->setRequired();    
               
        $form->addHidden('sup_email', $userIdentity->email );      
        $form->addHidden('sup_city', $userIdentity->city );         
        $form->addHidden('sup_street', $userIdentity->street );        
        $form->addHidden('sup_zip', $userIdentity->zip );       
        $form->addHidden('sup_ic', $userIdentity->ic );        
        $form->addHidden('sup_dic', $userIdentity->dic );      
        $form->addHidden('sup_an', $userIdentity->an );      
        $form->addHidden('sup_tradeoffice', $userIdentity->tradeoffice );     
        $form->addHidden('sup_name', $userIdentity->username );     

        $form->addHidden('sub_name', 'Fresh Services, s.r.o.' );         
        $form->addHidden('sub_city', 'Praha 4' );         
        $form->addHidden('sub_street', 'Nad Ostrovem 791/8' );     
        $form->addHidden('sub_zip', '147 00' );     
        $form->addHidden('sub_ic', '28180208' );     
        $form->addHidden('sub_dic', 'CZ28180208');    

        if ($this->invoice) {
            $form->setDefaults($this->invoice->toArray());
        }

        $form->addSubmit('send', 'Uložit fakturu');
        $form->onSuccess[] = [$this, 'costFormSucceeded'];
        return $form;
    }
    /** 
    * action to edit invoice
    * @param $id - id of invoice
    */
    public function actionDetail($id)
    {
        $invoice = $this->database->table('invoices')->get($id);
        if (!$invoice) {
            $this->error('Faktura nebyla nalezena');
        }
    }
    /** 
    * action to delete invoice
    * @param $id - id of invoice
    */
    public function actionDelete($id)
    {
        // pokud má faktura už náklady tak je nastavím jako nevyfakturované
        $user = $this->getUser();
        $userId = $user->getIdentity()->getId();
        $data = array('invoice_id' => NULL);
        $where = array("user_id" => $userId, 'invoice_id' => $id);   
        $this->database->Table('costs')->where($where)->update($data);
        // pak smažu fakturu z DB
        $this->database->table('invoices')->where('id', $id)->delete();         
        $this->flashMessage('Faktura smazána', 'success');   
        $this->redirect('Invoice:default');
    }
    /** 
    * Save invoice values - edit + create
    */ 
    public function costFormSucceeded(Form $form)
    {
        $values = $form->getValues();
        $this->database->table('invoices')->insert($values);
        $this->flashMessage('Faktura uložena', 'success');
        $this->redirect('Invoice:default');
    }
    /**
    * handler to assing unassigned user costs to current invoice
    * @param $id - id of invoice
    */
    public function handleAssignCostsToInvoice($id) {
        $user = $this->getUser();
        $userId = $user->getIdentity()->getId();
        $data = array('invoice_id' => $id);
        $where = array("user_id" => $userId, 'invoice_id' => NULL);      
        $this->database->Table('costs')->where($where)->update($data);
    }
    /**
    * handler to create PDF invoice and download it 
    * @param $id - id of invoice
    */
    public function handlePrintInvoiceToPdf($id) {
        $template = $this->createTemplate();
        $template->setFile(__DIR__ . "/templates/Invoice/createPdf.latte");

        $invoice = $this->database->table('invoices')->get($id);
        $template->invoice = $invoice;

        $template->invoiceItems = $this->database->table('costs')->where('invoice_id', $id);
        $template->totalPrice = $this->database->table('costs')->where('invoice_id', $id)->sum('price');

        $date = clone $invoice->date;
        $date->modify('+14 days');
        $template->expirationdate = $date;

        $pdf = new \Joseki\Application\Responses\PdfResponse($template);
        // optional
        $pdf->documentTitle = $invoice->number; // creates filename according invoice number
        $pdf->pageFormat = "A4"; // wide format
        $pdf->getMPDF()->setFooter("| Generated by FRESH cost |"); // footer

        $pdf->setSaveMode(\Joseki\Application\Responses\PdfResponse::DOWNLOAD);
        $pdf->send($this->getHttpRequest(), $this->getHttpResponse());
    }
}
