<?php

namespace App\Presenters;

use Nette;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

class HomepagePresenter extends BasePresenter
{


    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    /**
    * render dashboard
    */
    public function renderDefault()
	{
	   $user = $this->getUser();
	   // hodnota nákladů celkem
	   $this->template->totalCosts = $this->database->table('costs')->where('user_id', $user->id)->where('invoice_id NOT', NULL)->sum('price');
	   // počet vystavených faktur
	   $this->template->totalInvoices = $this->database->table('invoices')->where('user_id', $user->id)->count("*");
	   // počet nepřiřazených nákladů
	   $this->template->unassignedCosts = $this->database->table('costs')->where('user_id', $user->id)->where('invoice_id', NULL)->count("*");
	   if ($user->isInRole('admin')) { // není uživatel v roli admina?
            // data for chart
	   		$this->allCosts();
	   		$this->allHours();
        } else {
        	$this->allCostsByUser();
	   		$this->allHoursByUser();
        }
	   
	}
	/**
    * get data of costs for admin
    */
	private function allCosts() {
		$data = $this->database->table('costs')->where('invoice_id NOT', NULL)->fetchAll();
		$result = ['01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0, '11'=>0, '12'=>0];
		foreach($data as $key => $value){
			 $month = $value->date->format("m");
			 $result[$month] += $value->price;
		}
		$ret = '[';
		$count = count($result);
		$i = 1;
		foreach( $result as $key =>$month) {
			$ret .= "['$key','$month']";
			
			if($i < $count) {
				$ret .=",";
			}
			$i++;
		}
		$ret .= ']';
		
	   $this->template->chart = $ret;
	}
	/**
    * get data of costs for user
    */
	private function allCostsByUser() {
		$user = $this->getUser();
		$data = $this->database->table('costs')->where('user_id', $user->id)->where('invoice_id NOT', NULL)->fetchAll();
		$result = ['01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0, '11'=>0, '12'=>0];
		foreach($data as $key => $value){
			 $month = $value->date->format("m");
			 $result[$month] += $value->price;
		}
		$ret = '[';
		$count = count($result);
		$i = 1;
		foreach( $result as $key =>$month) {
			$ret .= "['$key','$month']";
			
			if($i < $count) {
				$ret .=",";
			}
			$i++;
		}
		$ret .= ']';
		
	   $this->template->chart = $ret;
	}
	/**
    * get data of hours for admin
    */
	private function allHours() {
		$data = $this->database->table('costs')->where('invoice_id NOT', NULL)->fetchAll();
		$result = ['01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0, '11'=>0, '12'=>0];
		foreach($data as $key => $value){
			 $month = $value->date->format("m");
			 $result[$month] += $value->hour;
		}
		$ret = '[';
		$count = count($result);
		$i = 1;
		foreach( $result as $key =>$month) {
			$ret .= "['$key','$month']";
			
			if($i < $count) {
				$ret .=",";
			}
			$i++;
		}
		$ret .= ']';
		
	   $this->template->chartSec = $ret;
	}
	/**
    * get data of hours for user
    */
	private function allHoursByUser() {
		$user = $this->getUser();
		$data = $this->database->table('costs')->where('user_id', $user->id)->where('invoice_id NOT', NULL)->fetchAll();
		$result = ['01'=>0, '02'=>0, '03'=>0, '04'=>0, '05'=>0, '06'=>0, '07'=>0, '08'=>0, '09'=>0, '10'=>0, '11'=>0, '12'=>0];
		foreach($data as $key => $value){
			 $month = $value->date->format("m");
			 $result[$month] += $value->hour;
		}
		$ret = '[';
		$count = count($result);
		$i = 1;
		foreach( $result as $key =>$month) {
			$ret .= "['$key','$month']";
			
			if($i < $count) {
				$ret .=",";
			}
			$i++;
		}
		$ret .= ']';
		
	   $this->template->chartSec = $ret;
	}
}
