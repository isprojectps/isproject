<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

class UserPresenter extends BasePresenter
{

    private $user;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    /** 
    * render table of users 
    */
    public function renderDefault()
	{
        $user = $this->getUser();
        parent::beforeRender();
        if (!$user->isInRole('admin')) { 
            $this->redirect('Homepage:');
        } 
	    $this->template->users = $this->database->table('users');
	        
	}
    /** 
    * render create form for user 
    */
    public function renderCreate()
    {
        $user = $this->getUser();
        parent::beforeRender();
        if (!$user->isInRole('admin')) { 
            $this->redirect('Homepage:');
        }           
    }
    /** 
    * render detail of user 
    */
   public function renderEdit($id)
    {
        $user = $this->getUser();
        parent::beforeRender();

        if ($user->id == $id) {

            $this->template->users = $this->user;

        } elseif ($user->isInRole('admin')){

            $this->template->users = $this->user;

        } else {

            $this->flashMessage('Nedostatečné oprávnění', 'danger');   
            $this->redirect('Homepage:');

        }
    } 
    /** 
    * Form create/edit user
    */
    protected function createComponentUserForm()
    {
        $form = new Form; // means Nette\Application\UI\Form
        
        $form->addText('username', 'Jméno a příjmení')
            ->setRequired(); 

        $form->addEmail('email', 'E-mail')
            ->setType('email')
            ->setAttribute('class', 'form-control')
            ->setRequired();

        $form->addText('city', 'Město'); 

        $form->addText('street', 'Ulice'); 

        $form->addText('zip', 'PSČ')
            ->setType('number')
            ->setRequired(FALSE)
            ->setAttribute('class', 'form-control')
            ->addRule(Form::INTEGER, 'PSČ musí být číslo')
            ->addRule(Form::MIN, 'PSČ musí být větší než 0', 0)
            ->addRule(Form::PATTERN, 'PSČ musí mít 5 číslic', '([0-9]\s*){5}');

        $form->addText('ic', 'IČ')
            ->setType('number')
            ->setRequired(FALSE)
            ->setAttribute('class', 'form-control')
            ->addRule(Form::INTEGER, 'IČ musí být číslo')
            ->addRule(Form::MIN, 'IČ musí být větší než 0', 0)
            ->addRule(Form::PATTERN, 'IČ musí mít 8 číslic', '([0-9]\s*){8}');

        $form->addText('dic', 'DIČ') 
            ->setRequired(FALSE)
            ->setAttribute('class', 'form-control')
            ->addRule(Form::PATTERN, 'DIČ musí být ve tvaru CZ a číslice', '(CZ.*[0-9])');

        $form->addText('an', 'Číslo účtu') 
            ->setRequired(FALSE)
            ->setAttribute('class', 'form-control')
            ->addRule(Form::PATTERN, 'Číslo účtu musí být ve tvaru číslo/číslo', '(.*[0-9].*/.*[0-9].*)');

        $form->addText('tradeoffice', 'Město živnostenského úřadu'); 

        $form->addPassword('password', 'Heslo')
            ->setRequired()
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
            ->addRule(Form::PATTERN, 'Musí obsahovat číslici', '.*[0-9].*'); 

        $user = $this->getUser();
        // if is user admin,can edit user role
        if ($user->isInRole('admin')) { 
            $userroles = [
            'admin',
            'user',
            ];

            $form->addSelect('role', 'Role')
                ->setRequired('Je nutné vybrat roli.')
                ->setAttribute('class', 'form-control')
                ->setPrompt('Vyberte roli')
                ->setItems($userroles, FALSE);
        } else {
            $form->addHidden('role', 'user' );
        }

        if ($this->user) {
            $form->setDefaults($this->user->toArray());
        }
        $form->addSubmit('send', 'Uložit uživatele');
        $form->onSuccess[] = [$this, 'userFormSucceeded'];
        return $form;
    }
    /** 
    * action to edit user
    */
    public function actionEdit($id)
    {
        $user = $this->user = $this->database->table('users')->get($id);
        if (!$user) {
            $this->error('Uživatel nebyl nalezen');
        }
    }
    /** 
    * action to delete user
    */
    public function actionDelete($id)
    {
        $this->database->table('users')->where('id', $id)->delete(); 
        
        $this->flashMessage('Uživatel smazán', 'success');   
        $this->redirect('User:default');
    }
    /** 
    * Save user values - edit + create
    */ 
    public function userFormSucceeded(Form $form)
    {
        $values = $form->getValues(); // převezmu si hodnoty z formuláře
        $values->password = Passwords::hash($values->password); // Zahashuje heslo
        //dump($this->user);exit;
        if ($this->user) { //existuje uživatel
            $this->user->update($values); // uprav jeho hodnoty
        }
        else {
            $this->database->table('users')->insert($values); // vytvoř nového uživatele
        }

        $this->flashMessage('Uživatel uložen', 'success');
        $this->redirect('User:default');
    }
}
