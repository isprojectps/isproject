<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class ProjectPresenter extends BasePresenter
{

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    /** 
    * Check user role before render
    */
    public function beforeRender()
    {
        $user = $this->getUser();
        parent::beforeRender();
        if (!$user->isInRole('admin')) { 
            $this->redirect('Homepage:');
        } 
        
    }
    /** 
    * render table of projects 
    */
    public function renderDefault()
	{
	    $this->template->projects = $this->database->table('projects');
	}
    /** 
    * render edit of project 
    * @param $id - id of project
    */
    public function renderEdit($id)
    {
        $project = $this->database->table('projects')->get($id);
        if (!$project) {
            $this->error('Stránka nebyla nalezena');
        }
        $this->template->project = $project;
        
    }
    /** 
    * render detail of project 
    * @param $id - id of project
    */
    public function renderDetail($id)
    {
        $project = $this->database->table('projects')->get($id);
        if (!$project) {
            $this->error('Stránka nebyla nalezena');
        }
        $this->template->project = $project;
        $this->template->costs = $this->database->table('costs')->where('project_id', $id);
        $totalCost = $this->database->table('costs')->where('project_id', $id)->sum('price');
        $this->template->totalcosts = $totalCost;

        $this->template->yield = $project->income - $totalCost;
    }
    /** 
    * Form create/edit project
    */
    protected function createComponentProjectForm()
    {
        $form = new Form; // means Nette\Application\UI\Form

        $form->addText('name', 'Název')
            ->setRequired(); 
        $form->addText('income', 'Výnos')
            ->addRule(Form::INTEGER, 'Výnos musí být číslo')
            ->addRule(Form::MIN, 'Výnos musí být větší než 0', 0)
            ->setAttribute('class', 'form-control')
            ->setRequired();           

        $form->addSubmit('send', 'Uložit projekt');
        $form->onSuccess[] = [$this, 'projectFormSucceeded'];
        return $form;
    }
    /** 
    * action to edit project
    * @param $id - id of project
    */
    public function actionEdit($id)
    {
        $project = $this->database->table('projects')->get($id);
        if (!$project) {
            $this->error('Projekt nebyl nalezen');
        }
        $this['projectForm']->setDefaults($project->toArray());

    }
    /** 
    * action to delete project
    * @param $id - id of project
    */
    public function actionDelete($id)
    {
        $projectId = $this->getParameter('id');

        if (!$projectId) {
            $this->error('Projekt nebyl nalezen');
        } else  {
            $project = $this->database->table('projects')->where('id', $projectId)->delete();
            
        }   
        $this->flashMessage('Projekt smazán', 'success');   
        $this->redirect('Project:default');
    }
    /** 
    * Save project values - edit + create
    * @param $form Nette\Application\UI\Form
    * @param $values Nette\Utils\ArrayHash
    */ 
    public function projectFormSucceeded($form, $values)
    {
        $projectId = $this->getParameter('id');

        if ($projectId) {
            $project = $this->database->table('projects')->get($projectId);
            $project->update($values);
        } else {
            $project = $this->database->table('projects')->insert($values);
        }

        $this->flashMessage('Projekt uložen', 'success');
        $this->redirect('Project:default');
    }
}
