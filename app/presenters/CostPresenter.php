<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class CostPresenter extends BasePresenter
{

    private $cost;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    /** 
    * render table of costs 
    */
    public function renderAllcosts()
    {
        $user = $this->getUser();

        $costs = $this->database->table('costs')
            ->order('date DESC'); 

        if (!$user->isInRole('admin')) { // není uživatel v roli admina?
            $id = $user->getIdentity()->getId();
            $costs->where('user_id', $id)->where('invoice_id', NULL); 
        }
        
        $this->template->costs = $costs; 
        
    }
    /** 
    * render table of user costs 
    */
    public function renderDefault()
	{
        $user = $this->getUser();

        $costs = $this->database->table('costs')
            ->order('date DESC'); 
            
        $id = $user->getIdentity()->getId();
        $costs->where('user_id', $id)->where('invoice_id', NULL); 
        
        $this->template->costs = $costs; 
        
	}
    /** 
    * render detail of cost
    * @param $id - id of cost 
    */
    public function renderEdit($id)
    {
        $cost = $this->database->table('costs')->get($id);
        if (!$cost) {
            $this->error('Stránka nebyla nalezena');
        }
        $this->template->cost = $cost;
    }
    /** 
    * Form create/edit cost
    */
    protected function createComponentCostForm()
    {
        $form = new Form; // means Nette\Application\UI\Form

        $user = $user = $this->getUser();
        $id = $user->getIdentity()->getId();

        $projects = $this->database->table('projects')->fetchPairs('id', 'name');

        $form->addSelect('project_id', 'Projekt', $projects)
            ->setRequired('Je nutné vybrat projekt.')
            ->setAttribute('class', 'form-control')
            ->setPrompt('Vyberte projekt');

        $form->addHidden('user_id', $user->getIdentity()->getId());

        $form->addText('name', 'Název')
            ->setRequired(); 

        $form->addText('price', 'Cena')
            ->addRule(Form::INTEGER, 'Cena musí být číslo')
            ->addRule(Form::MIN, 'Cena musí být větší než 0', 0)
            ->setAttribute('class', 'form-control')
            ->setRequired();

        $form->addText('hour', 'Počet hodin')
            ->addRule(Form::FLOAT, 'Počet hodin musí být číslo')
            ->addRule(Form::MIN, 'Počet hodin musí být větší než 0', 0)
            ->setAttribute('class', 'form-control')
            ->setRequired();       

        $form->addSubmit('send', 'Uložit náklad');
        $form->onSuccess[] = [$this, 'costFormSucceeded'];
        return $form;
    }
    /** 
    * action to edit cost
    * @param $id - id of cost
    */
    public function actionEdit($id)
    {
        $cost = $this->database->table('costs')->get($id);
        if (!$cost) {
            $this->error('Náklad nebyl nalezen');
        }
        $this['costForm']->setDefaults($cost->toArray());

    }
    /** 
    * action to delete cost
    * @param $id - id of cost
    */
    public function actionDelete($id)
    {

        $this->database->table('costs')->where('id', $id)->delete();
              
        $this->flashMessage('Náklad smazán', 'success');   
        $this->redirect('Cost:default');
    }
    /** 
    * Save Cost values - edit + create
    */ 
    public function costFormSucceeded($form, $values)
    {

        $costId = $this->getParameter('id');

        if ($costId) {
            $cost = $this->database->table('costs')->get($costId);
            $cost->update($values);
        } else {
            $this->database->table('costs')->insert($values);
        }

        $this->flashMessage('Náklad uložen', 'success');
        $this->redirect('Cost:default');
    }
}
