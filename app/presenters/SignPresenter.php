<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class SignPresenter extends Nette\Application\UI\Presenter
{

    protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addText('email', 'Uživatelské jméno:')
            ->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
    }
    /**
	 * @param $form Nette\Application\UI\Form
	 * @param $values Nette\Utils\ArrayHash
	 */
	public function signInFormSucceeded($form, $values)
	{
	    try {
	        $this->getUser()->login($values->email, $values->password);
	        $this->redirect('Homepage:');

	    } catch (Nette\Security\AuthenticationException $e) {
	        $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
	    }
	}
	public function actionOut()
	{
	    $this->getUser()->logout();
	    $this->flashMessage('Odhlášení bylo úspěšné.');
	    $this->redirect('Homepage:');
	}
}
