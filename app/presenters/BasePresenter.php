<?php
namespace App\Presenters;
    
use Nette;
use Nette\Application\UI\Form;

/**
 * Base presenter for all application presenters.
 *
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/** @var Nette\Database\Context @inject */
    public $database;
    
    public function startup()
    {
    	parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
	        $this->redirect('Sign:in');
	    } 
        
    }
    public function beforeRender()
    {
        $user = $this->database->table('users')->get($this->user->id);
        $this->template->username = $user->username;
    }
}
